document.addEventListener("DOMContentLoaded", function () {
	document.getElementById('recla').addEventListener('submit', validarDatos);
});

function validarDatos(evento){
	evento.preventDefault();

	let nom, ape, mail, direc, cel;
	nom = document.getElementById('nombre').value
	ape = document.getElementById('apellidos').value;
	mail = document.getElementById('mail').value;
	direc = document.getElementById('dirección').value;
	cel = document.getElementById('celular').value;
	come = document.getElementById('comentanos').value;

	if(nom==""){
		return	alert("El nombre es un campo obligatorio a completar");
		
	}
	if(ape==""){
		return alert("Los apellidos son un campo obligatorio a completar");
		
	}
	if(mail==""){
		return	alert("Debes colocar el mail para recibir tu reclamo");
		
	}
	if(direc==""){
		return	alert("Debes colocar la dirección para proceder con tu reclamo");
		
	}
	if(cel==""){
		return	alert("El celular es obligatorio a colocar como medio de contacto");
		
	}
	else if(isNaN(cel)){
		return alert("El celular debe ser numerico");
		
	}
	else if(cel.length != 9){
		
		return alert("El celular debe tener 9 dígitos")
	}


	if(come==""){
		return alert("Debes detallarnos mas sobre tu reclamo en la seccion de comentanos mas sobre tu reclamo");
		
		}
	
		document.recla.submit();

	// this.submit();
}